import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import './App.css';
import MainApp from './components/MainApp';
import Modal from './components/common/Modal';
import VideoPlayer from './components/common/VideoPlayer';
import WindowFrame from './components/common/WindowFrame';

function App() {
  return (
    <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
      <WindowFrame />
      <MainApp />
      <Modal />
      <VideoPlayer />
    </Provider>
  );
}

export default App;

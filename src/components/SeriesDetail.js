import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import EpisodeListEntry from './common/EpisodeListEntry';
import LoadingSpinner from './common/LoadingSpinner';

import './styles/EpisodeListEntry.scss';
import './styles/SeriesDetail.scss';

class SeriesDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {descriptionExpanded: false};
    }

    // componentDidMount() {
    //     this.props.loadSeries(this.props.content.currentSeries.series_id, this.props.authentication.sessionId);
    // }

    componentDidUpdate() {
        console.log("SeriesDetail UPDATED WITH");
        console.log(this.props.content.currentMediaList);
        //this.props.loadSeries(this.props.content.currentSeries.series_id, this.props.authentication.sessionId);
    }

    // shouldComponentUpdate(nextProps) {
    //     return (this.props.content.currentMediaList !== nextProps.content.currentMediaList || this.props.content.currentSeries !== nextProps.content.currentSeries);
    // }

    render() {
        const bgStyle = {
            backgroundImage: `url(${this.props.content.currentSeries.portrait_image.large_url})`
        };
        return (
            <div id="seriesDetailContainer">
                <div id="seriesInformationContainer">
                    <div id="seriesCoverImage"><img src={this.props.content.currentSeries.portrait_image.large_url} /></div>
                    <div id="seriesInformation">
                        <div id="seriesTitle">{this.props.content.currentSeries.name}</div>
                        <div id="seriesDescription" class={this.state.descriptionExpanded ? "expanded" : ""}>
                            {this.props.content.currentSeries.description}
                        </div>
                        <button id="expandButton" onClick={this.expandDescription.bind(this)}><div id="expandIconContainer"
                            class={this.state.descriptionExpanded ? "expanded" : ""} /></button>
                    </div>
                </div>
                <div id="seriesEpisodesContainer">
                    <div id="episodeLabel">Episodes</div>
                    <div id="episodesList">
                    {!this.props.content.loadingStatus.episodes ?
                        this.props.content.currentMediaList.map((episodes, index) => {
                            return <div id="collectionContainer" key={index} style={{order: episodes.season}}>
                                    <div id="collectionTitle">{episodes.name}</div>
                                {episodes.episodeList.map((ep) => {
                                    console.log("SeriesDetail RENDERING EpisodeListEntry " + ep.name);
                                    return <EpisodeListEntry key={ep.media_id} mediaId={ep.media_id} episodeName={ep.name} locale={episodes.locale} episodeNumber={ep.episode_number} bgImage={ep.screenshot_image.large_url} />
                                })}

                            </div>
                        }) : <LoadingSpinner />
                    }
                    </div>
                </div>
            </div>
        )
    }

    expandDescription() {
        this.setState((state, props) => {
            return {descriptionExpanded: !state.descriptionExpanded}
        });
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(SeriesDetail);
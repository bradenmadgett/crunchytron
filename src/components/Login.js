import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import reducers from '../reducers';

import './styles/Login.scss';
import LoadingSpinner from './common/LoadingSpinner';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        console.log(this.props);
        return (
            !this.props.content.loadingStatus.session ? 
            <div id="LoginForm">
                <div id="left">
                    <div id="welcomeLabel">Welcome to <br/> Crunchytron </div>
                </div>
                <div id="right">
                    <input placeholder="username" onInput={this.onEmailChange.bind(this)} />
                    <input placeholder="password" type="password" onInput={this.onPasswordChange.bind(this)} />
                    <button onClick={this.loginUser.bind(this)}>SIGN IN</button>
                </div>
                
            </div> : <LoadingSpinner />
        )
    }

    componentDidMount() {
        console.log("LOGIN DID MOUNT");
        if(this.props.authentication.sessionId == null)
        {
            this.props.startSession();
        }
    }

    onEmailChange(event){
        this.props.updateAccount(event.target.value);
        console.log(this.authentication);
    }

    onPasswordChange(event) {
        this.props.updatePassword(event.target.value);
        console.log(this.authentication);
    }

    loginUser() {
        //TODO start new session after logout
        console.log("login pressed");
        this.props.loginUser({account: this.props.authentication.account, password: this.props.authentication.password, sessionId: this.props.authentication.sessionId});
        console.log(this.authentication);
    }
}

const mapStateToProps = state => {
    return state;
};

export default connect(mapStateToProps, actions)(LoginForm);
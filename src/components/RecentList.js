import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import EpisodeListEntry from './common/EpisodeListEntry';
import LoadingSpinner from './common/LoadingSpinner';

class RecentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.getRecent(this.props.authentication.sessionId);
    }

    render() {
        return (
            <div id="sectionWrapper">
                <div id="sectionTitle">RECENT</div>
                <div id="listSection">
                            {!this.props.content.loadingStatus.recent ? (this.props.content.recentShows.length > 0 ? this.props.content.recentShows.map((show, index) => {
                return <EpisodeListEntry bgImage={show.media.screenshot_image.large_url} key={show.media.media_id} mediaId={show.media.media_id} episodeNumber={show.media.episode_number} episodeName={show.media.name} /> 
            }) : <div id="emptyLabel">You have no recent shows.</div>) : <LoadingSpinner />}
                </div>
            </div>
            
            
        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(RecentList);
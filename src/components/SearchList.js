import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import SeriesListEntry from './common/SeriesListEntry';
import LoadingSpinner from './common/LoadingSpinner';

import './styles/SearchList.scss';

class SearchList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        
    }

    render() {
        return (
            this.props.content.searchBarFocused ? <div id="searchListContainer" onClick={this.searchBarFocused.bind(this)}>
                <div id="listContainer">
                    {!this.props.content.loadingStatus.search ? (this.props.content.searchResults.length > 0 ? this.props.content.searchResults.map((result) => {
                        return <SeriesListEntry bgImage={result.portrait_image.large_url} key={result.series_id} series={result} name={result.name} />
                    }) : <div id="noResults">No results found</div>) : <LoadingSpinner />} 
                </div>
            </div> : ""
        )
    }

    searchBarFocused(event) {
        this.props.setSearchbarFocus(false);
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(SearchList);
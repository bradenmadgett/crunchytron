import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render(){
        return (
            <input id="searchBar" placeholder="SEARCH" onFocus={this.searchBarFocused.bind(this)} onKeyPress={this.submitSearchIfEnter.bind(this)} />
        )
    }

    searchBarFocused(event) {
        this.props.setSearchbarFocus(true);
    }

    // searchBarBlurred(event) {
    //     this.props.setSearchbarFocus(false);
    // }

    submitSearchIfEnter(event) {
        if(event.key === "Enter")
        {
            console.log("SUBMIT SEARCH " + event.target.value);
            this.props.search(this.props.authentication.sessionId, event.target.value);
        }  
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(SearchBar);
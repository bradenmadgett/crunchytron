import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import SeriesListEntry from './common/SeriesListEntry';
import LoadingSpinner from './common/LoadingSpinner';

class QueueList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    
    componentDidMount() {
        this.props.getQueue(this.props.authentication.sessionId);
    }

    render() {
        return (
            <div id="sectionWrapper">
                <div id="sectionTitle">QUEUE</div>
                <div id="listSection">
                    {!this.props.content.loadingStatus.queue ? (this.props.content.queuedShows.length > 0 ? this.props.content.queuedShows.map((show, index) => {
                        return <SeriesListEntry bgImage={show.series.portrait_image.large_url} key={show.series.series_id} series={show.series} name={show.series.name} /> 
                    }) : <div id="emptyLabel">You have no shows queued.</div>) : <LoadingSpinner />
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(QueueList);
import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import SeriesListEntry from './common/SeriesListEntry';
import LoadingSpinner from './common/LoadingSpinner';

class BrowseList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {filtersExpanded: false, filter: "popular"};
        console.log("CONTRUCTOR FILTER " + this.state.filter);
    }

    componentDidMount() {
        //this.props.clearBrowse();
        if(this.props.content.currentBrowseList.length === 0)
        {
            console.log("COMPONENT DID MOUNT FETCH FILTER " + this.state.filter);
            this.props.getBrowse(this.props.authentication.sessionId, this.state.filter, 50, null);
        }
    }

    render() {
        return (
            <div id="sectionWrapper">
                <div id="sectionTitle">BROWSE <button id="filterButton" onClick={this.expandFilters.bind(this)}/></div>
                <div id="filtersSection" class={this.state.filtersExpanded ? "open" : ""}>
                    <button id="filter" class={this.state.filter === "popular" ? "selected" : ""} onClick={this.updateBrowse.bind(this, "popular")}>Popular</button>
                    <button id="filter" class={this.state.filter === "simulcast" ? "selected" : ""} onClick={this.updateBrowse.bind(this, "simulcast")}>Simulcasts</button>
                    <button id="filter" class={this.state.filter === "updated" ? "selected" : ""} onClick={this.updateBrowse.bind(this, "updated")}>Updated</button>
                    <button id="filter" class={this.state.filter === "alpha" ? "selected" : ""} onClick={this.updateBrowse.bind(this, "alpha")}>Alphabetical</button>
                    <button id="filter" class={this.state.filter === "newest" ? "selected" : ""} onClick={this.updateBrowse.bind(this, "newest")}>Newest</button>
                </div>
                <div id="listSection">
                    {this.props.content.currentBrowseList.map((show, index) => {
                        return <SeriesListEntry bgImage={show.portrait_image.large_url} key={show.series_id} series={show} name={show.name} /> 
                    })}
                    {this.props.content.loadingStatus.browse ? <LoadingSpinner /> : ""}
                </div>
                {this.props.content.currentBrowseList.length > 0 ? <div id="loadMoreButtonContainer"><button id="loadMoreButton" onClick={this.fetchNextShows.bind(this)}>Load More<div id="overlay" /></button></div> : (!this.props.content.loadingStatus.browse ? <div id="emptyLabel">There are no shows to browse.</div> : "")}
            </div>
        )
    }

    expandFilters() {
        this.setState({filtersExpanded: !this.state.filtersExpanded});
    }

    fetchNextShows() {
        console.log("Fetch next shows");
        console.log("FILTER " + this.state.filter);
        this.props.getBrowse(this.props.authentication.sessionId, this.state.filter, 50, this.props.content.currentBrowseList.length);
    }

    updateBrowse(filter) {
        if (this.state.filter === filter)
        {
            console.log("Filter already selected");
        }
        else {
            this.setState({filter: filter}, () => {
                console.log("UPDATE BROWSE STATE IS " + this.state.filter);
                this.props.clearBrowse();
                this.props.getBrowse(this.props.authentication.sessionId, filter, 50, 0);
            });
        }
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(BrowseList);
import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore } from 'redux';
import reducers from '../reducers';
import * as actions from '../actions';
import LoginForm from './Login';
import Home from './Home';

import './styles/MainApp.css';

class MainApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return this.props.authentication.isLoggedIn ? <Home /> : <LoginForm />;
    }
}

const mapStateToProps = state => {
    return state;
}

export default connect(mapStateToProps, actions)(MainApp);
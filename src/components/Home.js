import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import Menu from './common/Menu';
import HeaderBar from './common/HeaderBar';
import ShowList from './ShowList';
import SeriesDetail from './SeriesDetail';
import NavigationButton from './common/NavigationButton';

import './styles/Home.scss';
import SearchList from './SearchList';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div id="homeContainer">
                <Menu />
                {/* <NavigationButton /> */}
                <HeaderBar />
                <div id="contentArea">
                    {this.props.content.mode === "series" ? <SeriesDetail /> : <ShowList />}
                    <SearchList />
                    {/* {this.props.content.searchBarFocused ? <SearchList /> : ""} */}
                </div>
            </div>
        );
    };
}

const mapStateToProps = state => {
    return state;
};

export default connect(mapStateToProps, actions)(Home);
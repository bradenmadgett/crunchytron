import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import '../styles/NavigationButton.scss';

class NavigationButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        const goBack = () => {
            this.props.goBack();
        }

        return (
            <button id="navigationButton" onClick={goBack}></button>
        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(NavigationButton);
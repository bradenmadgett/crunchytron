import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class EpisodeListEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const bgStyle = {
            backgroundImage: `url(${this.props.bgImage})`
        };

        return (
            <div id="episodeListEntry" key={this.props.key} style={bgStyle} onClick={this.playEpisode.bind(this)}>
                <div id="overlay" />
                <div id="episodeInfoBar">
                    <div id="episodeNumber">Episode {this.props.episodeNumber}</div>
                    <div id="episodeName">{this.props.episodeName}</div>
                </div>
            </div>
        )
    }

    playEpisode() {
        console.log("PLAY " + this.props.mediaId + " WITH LOCALE " + this.props.locale);
        this.props.loadVideo(this.props.authentication.sessionId, this.props.authentication.deviceId, this.props.authentication.deviceType, this.props.mediaId, this.props.locale);
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(EpisodeListEntry);
import '../styles/LoadingSpinner.scss';
import React from 'react';

const LoadingSpinner = (props) => {
    return (
        <div id="spinnerContainer">
            {/*FOLLOWING CSS FROM https://loading.io/css/ */}
            <div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
        
    )
}

export default LoadingSpinner;
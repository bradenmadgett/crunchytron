import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import SearchBar from '../SearchBar';
import NavigationButton from './NavigationButton';

import '../styles/HeaderBar.scss';

const os = window.require('os');


class HeaderBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div id="headerBarContainer" className={[this.props.content.menuIsOpen ? "menuOpen" : "", os.platform()].join(' ')}>
                <div id="menuToggleButton" className={this.props.content.menuIsOpen ? "menuOpen" : ""} onClick={this.toggleMenu}>
                    <div className="line" id="one" />
                    <div className="line" id="two" />
                    <div className="line" id="three" />
                </div>
                <NavigationButton />
                <SearchBar />
                <div id="userArea">
                    <div id="profilePicture" />
                    <div id="userInfoArea">
                        <div id="username">{this.props.authentication.account}</div>
                        <div id="userStatus"><p>{this.props.authentication.user.premium !== "" ? "PREMIUM" : "FREE"}</p></div>
                    </div>
                </div>
            </div>
        )
    }

    toggleMenu = () => {
        console.log("Menu toggled " + this.props.content.menuIsOpen);
        this.props.toggleMenu(!this.props.content.menuIsOpen);
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(HeaderBar);
import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import '../styles/Modal.scss';

class Modal extends React.Component {
    constructor(props){
        super(props);
        this.state = {visible: true};
    };

    render() {
        return (
            this.props.content.isError ?
                <div id="modalContainer" class="error" onClick={this.hideModal.bind(this)}>
                    <div id="modalMessage">{this.props.content.errorMessage || "ERROR"}</div>
                </div>
                : null
        );
    };

    displayModal(){
        this.setState({visible: true});
    }

    hideModal(){
        this.props.setErrorState(false, null);
    }
};

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(Modal);
import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import '../styles/WindowFrame.scss';
const electron = window.require('electron');
const os = window.require('os');

class WindowFrame extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isMaximized: false};
    }

    render() {
        return (
            <div id="windowFrameContainer" className={os.platform()}>
                <div id="buttonContainer">
                    <div id="minimize" className="button" onClick={this.minimizeApplication.bind(this)}></div>
                    <div id="maximize" className="button" onClick={this.maximizeApplication.bind(this)}></div>
                    <div id="close" className="button" onClick={this.closeApplication.bind(this)}></div>
                </div>
            </div>
        )
    }

    closeApplication = () => {
        console.log("Close app");
        electron.remote.getCurrentWindow().close();
    }

    minimizeApplication = () => {
        console.log("Minimize app");
        electron.remote.getCurrentWindow().minimize();
    }

    maximizeApplication = () => {
        console.log("Maximize app");
        if(os.platform() === "darwin") {
            if(this.state.isMaximized) {
                electron.remote.getCurrentWindow().setFullScreen(false);
                this.setState({isMaximized: false});
            } else {
                electron.remote.getCurrentWindow().setFullScreen(true);
                this.setState({isMaximized: true});
            }
        } else {
            if(this.state.isMaximized) {
                electron.remote.getCurrentWindow().unmaximize();
                this.setState({isMaximized: false});
            } else {
                electron.remote.getCurrentWindow().maximize();
                this.setState({isMaximized: true});
            }
        }
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(WindowFrame);
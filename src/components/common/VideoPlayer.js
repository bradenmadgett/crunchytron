import React from 'react';
import { connect } from 'react-redux';
import hls from 'hls.js';
import * as actions from '../../actions';

import '../styles/VideoPlayer.scss';

class VideoPlayer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {currentVideo: this.props.content.currentVideo};
    }

    componentDidUpdate() {
        console.log("ComponentDidMount");
        //this.setState({currentVideo: this.props.content.currentVideo});
        this.loadVideo();
    }

    shouldComponentUpdate(nextProps) {
        return !(this.props.content.currentVideo === nextProps.content.currentVideo);
    }

    loadVideo() {
        if(this.props.content.currentVideo)
        {
            const videoTag = document.getElementById("videoPlayer");
            if(hls.isSupported()) {
                var HLS = new hls();
                HLS.loadSource(this.props.content.currentVideo.streamUrl);
                HLS.attachMedia(videoTag);
                HLS.on(hls.Events.MANIFEST_PARSED, () => {
                    videoTag.play()
                    this.props.addToHistory(this.props.authentication.sessionId, this.props.content.currentVideo.mediaId);
                });
            }
        }
    }

    render() {
        console.log("Render");
        return (
            this.props.content.currentVideo ? <div id="videoPlayerContainer"><video id="videoPlayer" controls></video><button id="exitButton" onClick={this.closeVideo.bind(this)} /></div> : <div />
        )
    }

    closeVideo() {
        const videoTag = document.getElementById("videoPlayer");
        videoTag.pause();
        videoTag.src="";
        this.props.unloadVideo();
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(VideoPlayer);
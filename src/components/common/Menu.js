import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';
import reducers from '../../reducers';

import '../styles/Menu.scss';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {options: [{title: "queue"}, {title: "browse"}]};
    }

    render() {
        return (
            <div id="menuContainer" className={this.props.content.menuIsOpen ? "open" : ""}>
                <div id="optionsArea">
                    {/* <div id="selectedMark" className={this.props.content.mode} /> */}
                    {this.state.options.map((option, index) => <button key={index} className="option" id={option.title} onClick={this.optionSelected.bind(this, option.title)}></button>)}
                    <button className="option" id="logOutButton" onClick={this.logOut}></button>
                </div>
            </div>
        )
    }

    // opts = this.state.options.map((option) => {
    //     return <div class="option" id={option.title}>{option.title}</div>;
    // });

    optionSelected = (option) => {
        console.log("option " + option + " selected");
        this.props.updateMode(option);
    }

    logOut = () => {
        this.props.logoutUser(this.props.authentication.auth, this.props.authentication.sessionId);
    }
}



const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(Menu);
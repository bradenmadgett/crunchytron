import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../actions';

class SeriesListEntry extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const bgStyle = {
            backgroundImage: `url(${this.props.bgImage})`
    
        };

        const loadSeries = () => {
            this.props.clearMediaList();
            this.props.loadSeries(this.props.series, this.props.authentication.sessionId);
        }

        return <div key={this.props.series.series_id} className="listEntry" style={bgStyle} onClick={loadSeries}>
                    {!this.checkIfQueued(this.props.series.series_id) ? <button id="addToQueueButton" class="queueButton" onClick={this.addToQueue.bind(this, this.props.series.series_id)}><div id="addIcon" class="icon"><div id="label">Add to Queue</div></div></button> :
                     <button id="queuedIndicator" class="queueButton" onClick={this.removeFromQueue.bind(this, this.props.series.series_id)}><div id="queuedIcon" class="icon"><div id="label">Remove From Queue</div></div></button> }
                    <div id="overlay"/>
                    <div id="titleBar"><p>{this.props.name}</p></div>
                </div>
    }

    checkIfQueued(seriesId) {
        var showAlreadyQueued = false;
        for(var i = 0; i < this.props.content.queuedShows.length; i++) {
            if(seriesId === this.props.content.queuedShows[i].series.series_id)
            {
                showAlreadyQueued = true;
                break;
            };
        }
        return showAlreadyQueued;
    }

    addToQueue(seriesId, event) {
        console.log("ADD " + seriesId + " TO QUEUE");
        event.stopPropagation(); //prevent button click from opening series detail
        this.props.addToQueue(this.props.authentication.sessionId, seriesId);
    }

    removeFromQueue(seriesId, event) {
        console.log("REMOVE " + seriesId + " FROM QUEUE");
        event.stopPropagation();
        this.props.removeFromQueue(this.props.authentication.sessionId, seriesId);
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(SeriesListEntry);
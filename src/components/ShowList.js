import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';

import SeriesListEntry from './common/SeriesListEntry';
import QueueList from './QueueList';
import RecentList from './RecentList';
import BrowseList from './BrowseList';
import LoadingSpinner from './common/LoadingSpinner';

import './styles/ShowList.scss';

class ShowList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div id="showListContainer">
                {this.props.content.mode === "queue" ? 
                    <div id="listContainer">
                        <RecentList />
                        <QueueList />
                    </div> :
                    <div id="listContainer">
                        <BrowseList />
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return state;
}

export default connect(mapStateToProps, actions)(ShowList);
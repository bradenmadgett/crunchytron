import { UPDATE_SELECTED_MODE, TOGGLE_MENU, ERROR, UPDATE_QUEUE, UPDATE_RECENT, LOAD_SERIES, LOAD_MEDIA_LIST, LOAD_VIDEO, UNLOAD_VIDEO, RESET_STATE, UPDATE_NAVIGATION, GO_BACK, UPDATE_BROWSE, UPDATE_LOADING_STATUS, UPDATE_SEARCH_RESULTS, SET_SEARCHBAR_FOCUS, LOAD_COLLECTIONS_LIST, CLEAR_MEDIA_LIST } from '../actions/types';

const init_state = {mode: "queue", loadingStatus: {queue: false, browse: false, recent: false, episodes: false, session: false, search: false}, menuIsOpen: false, isError: false, searchBarFocused: false, errorMessage: null, queuedShows: [], recentShows: [], currentSeries: null, collectionsList:[], currentMediaList: [], currentBrowseList: [], searchResults: [], currentVideo: null, navigation: []};

export default (state = init_state, action) => {
    switch(action.type) {
        case UPDATE_SELECTED_MODE:
            const newState = {...state, mode: action.payload};
            console.log(newState);
            return newState; //return new state object so redux knows our state changed
        case TOGGLE_MENU:
            return {...state, menuIsOpen: action.payload};
        case UPDATE_QUEUE:
            console.log("UPDATE QUEUE " + action.payload);
            return {...state, queuedShows: action.payload.queuedShows};
        case UPDATE_RECENT:
            console.log("UPDATE RECENT " + action.payload);
            return {...state, recentShows: action.payload.recentShows};
        case ERROR:
            return {...state, isError: action.payload.isError, errorMessage: action.payload.message}
        case LOAD_SERIES:
            console.log("LOAD SERIES " + action.payload);
            return {...state, currentSeries: action.payload}
        case CLEAR_MEDIA_LIST:
            console.log("CLEAR MEDIA LIST");
            return {...state, currentMediaList: []};
        case LOAD_MEDIA_LIST:
            console.log("LOAD MEDIA LIST " + JSON.stringify(action.payload));
            var newContent = new Array(...state.currentMediaList);
            newContent.push({name: action.payload.collectionName, season: action.payload.seasonNumber, episodeList: action.payload.data});
            return {...state, currentMediaList: newContent};
        case LOAD_VIDEO:
            console.log("LOAD VIDEO");
            return {...state, currentVideo: {streamUrl: action.payload.streamData.data.stream_data.streams[0].url, mediaId: action.payload.mediaId}};
        case UNLOAD_VIDEO:
            console.log("UNLOAD VIDEO");
            return {...state, currentVideo: null};
        case RESET_STATE:
            console.log("RESET STATE");
            return {...init_state};
        case UPDATE_NAVIGATION:
            console.log("UPDATE NAVIGATION");
            var updatedNavigation; 
            if(state.navigation.length > 0)
            {
                updatedNavigation = new Array(...state.navigation);
                updatedNavigation.push({mode: state.mode, queuedShows: state.queuedShows, recentShows: state.recentShows, currentSeries: state.currentSeries, collectionsList: state.collectionsList, currentMediaList: state.currentMediaList, currentVideo: state.currentVideo});
            } else {
                updatedNavigation = [];
                updatedNavigation.push({mode: state.mode, queuedShows: state.queuedShows, recentShows: state.recentShows, currentSeries: state.currentSeries, collectionsList: state.collectionsList, currentMediaList: state.currentMediaList, currentVideo: state.currentVideo});
            }
            console.log(updatedNavigation);
            return {...state, navigation: updatedNavigation};
        case UPDATE_BROWSE:
            console.log("UPDATE BROWSE");
            var newBrowseList = [];
            if(action.payload.action === "add") {
                newBrowseList = new Array(...state.currentBrowseList);
                newBrowseList.push(...action.payload.showList);
            }
            else if(action.payload.action === "clear"){
                console.log("BROWSE LIST CLEARED");
            }
            return {...state, currentBrowseList: newBrowseList};
        case UPDATE_SEARCH_RESULTS: 
            console.log("UPDATE SEARCH RESULTS");
            return {...state, searchResults: action.payload};
        case LOAD_COLLECTIONS_LIST:
            console.log("LOAD COLLECTIONS LIST");
            return {...state, collectionsList: action.payload};
        case UPDATE_LOADING_STATUS:
            console.log("UPDATE LOADING STATUS " + action.payload.name + " TO " + action.payload.status);
            var newLoadingStatus = state.loadingStatus; 
            switch(action.payload.name) {
                case "queue":
                    newLoadingStatus = {...newLoadingStatus, queue: action.payload.status};
                    break;
                case "recent":
                    newLoadingStatus = {...newLoadingStatus, recent: action.payload.status};
                    break;
                case "browse":
                    newLoadingStatus = {...newLoadingStatus, browse: action.payload.status};
                    break;
                case "episodes":
                    newLoadingStatus = {...newLoadingStatus, episodes: action.payload.status};
                    break;
                case "session":
                    newLoadingStatus = {...newLoadingStatus, session: action.payload.status};
                    break;
                case "search":
                    newLoadingStatus = {...newLoadingStatus, search: action.payload.status};
                    break;
                default:
                    break;
            }
            return {...state, loadingStatus: {...newLoadingStatus}};
        case SET_SEARCHBAR_FOCUS:
            console.log("SEARCHBAR FOCUS " + action.payload);
            return {...state, searchBarFocused: action.payload};
        case GO_BACK:
            console.log("GO BACK");
            const currentNavigation = new Array(...state.navigation);
            console.log(currentNavigation);
            if(currentNavigation.length <= 1)
            {
                return {...init_state, queuedShows: state.queuedShows, recentShows: state.recentShows, currentVideo: state.currentVideo};
            }
            else {
                const prevState = currentNavigation.pop();
                if(prevState.mode === "browse")
                {
                    //add clearing functionality if desired
                    return {...init_state, queuedShows: state.queuedShows, recentShows: state.recentShows, currentVideo: state.currentVideo};
                }
                return {...state, ...currentNavigation[currentNavigation.length - 1], currentVideo: state.currentVideo, navigation: currentNavigation};
            }
        default:
            return state;
    }
}
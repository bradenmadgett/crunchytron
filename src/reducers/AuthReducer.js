import { UPDATE_ACCOUNT, UPDATE_LOGGED_IN, UPDATE_PASSWORD, UPDATE_SESSION, UPDATE_AUTH, LOGOUT_USER, RESET_STATE } from '../actions/types';

const init_state = {isLoggedIn: false, account: null, password: null, sessionId: null, deviceType: null, deviceId: null, auth: null, user: null};

export default (state = init_state, action) => {
    switch(action.type) {
        case UPDATE_ACCOUNT:
            return {...state, account: action.payload}; //return new state object so redux knows our state changed
        case UPDATE_LOGGED_IN:
            return {...state, isLoggedIn: action.payload};
        case UPDATE_PASSWORD:
            return {...state, password: action.payload};
        case UPDATE_SESSION:
            return {...state, sessionId: action.payload.sessionId, deviceType: action.payload.deviceType, deviceId: action.payload.deviceId};
        case UPDATE_AUTH:
            return {...state, auth: action.payload.auth, isLoggedIn: action.payload.isLoggedIn, user: action.payload.user}
        case LOGOUT_USER:
            return {...init_state}
        case RESET_STATE:
            return {...init_state}
        default:
            return state;
    }
}
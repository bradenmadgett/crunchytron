import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import ContentReducer from './ContentReducer';

export default combineReducers({
    authentication: AuthReducer,
    content: ContentReducer
})
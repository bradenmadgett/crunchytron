import { UPDATE_ACCOUNT, UPDATE_LOGGED_IN, UPDATE_PASSWORD, UPDATE_SESSION, UPDATE_AUTH, UPDATE_SELECTED_MODE, TOGGLE_MENU, 
    LOGOUT_USER, ERROR, UPDATE_QUEUE, UPDATE_RECENT, UPDATE_BROWSE, LOAD_SERIES, LOAD_MEDIA_LIST, LOAD_VIDEO, UNLOAD_VIDEO, RESET_STATE, UPDATE_NAVIGATION, UPDATE_LOADING_STATUS, GO_BACK, UPDATE_SEARCH_RESULTS, SET_SEARCHBAR_FOCUS, LOAD_COLLECTIONS_LIST, CLEAR_MEDIA_LIST } from './types';
import axios from 'axios';
import uuidv4 from 'uuid/v4';

export const updateAuth = (authKey) => {
    return {
        type: UPDATE_AUTH,
        payload: authKey
    };
};

export const updateAccount = (accountText) => {
    return {
        type: UPDATE_ACCOUNT,
        payload: accountText
    };
};

export const updateLoggedIn = (isLoggedIn) => {
    return {
        type: UPDATE_LOGGED_IN,
        payload: isLoggedIn
    };
};

export const updatePassword = (password) => {
    return {
        type: UPDATE_PASSWORD,
        payload: password
    };
};

export const startSession = () => {
    return (dispatch) => {
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "session", status: true}});
        const uuid = uuidv4();
        axios.get('https://api.crunchyroll.com/start_session.0.json?access_token=LNDJgOit5yaRIWN&device_type=com.crunchyroll.windows.desktop&device_id=' + uuid)
        .then(response => {
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "session", status: false}});
            const sessionInfo = {sessionId: response.data.data.session_id, 
                deviceType: response.data.data.device_type, deviceId: response.data.data.device_id};
            console.log(response);
            dispatch({type: UPDATE_SESSION, payload: sessionInfo});
        });
    };
};

export const loginUser = ({account, password, sessionId}) => {
    return (dispatch) => {
        //make login request
        const builtUrl = "https://api.crunchyroll.com/login.0.json?account=" + account + "&password=" + password + "&session_id=" + sessionId;
        axios.get(builtUrl)
        .then(response => {
            console.log(response.data);
            if(response.data.error) {
                console.log(response.data.message)
                dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: UPDATE_AUTH, payload: {auth: response.data.data.auth, isLoggedIn: true, user: response.data.data.user}});
            }
        });
    };
};

export const updateMode = (selectedMode) => {
    return(dispatch) =>  {
        dispatch({type: UPDATE_SELECTED_MODE,
        payload: selectedMode});

        dispatch({type: UPDATE_NAVIGATION, payload: selectedMode});
    };
};

export const toggleMenu = (menuIsOpen) => {
    return {
        type: TOGGLE_MENU,
        payload: menuIsOpen
    };
};

export const logoutUser = (auth, sessionId) => {
    return (dispatch) => {
        //make logout request
        const builtUrl = "https://api.crunchyroll.com/logout.0.json?&auth=" + auth + "&session_id=" + sessionId;
        axios.post(builtUrl)
        .then(response => {
            console.log(response.data);
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}})
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                }
            }
            else {
                dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                dispatch({type: RESET_STATE, payload: {}});
            }
        });
    };
}

export const getQueue = (sessionId) => {
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/queue.0.json?session_id=" + sessionId;
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "queue", status: true}});
        axios.get(builtUrl)
        .then(response => {
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "queue", status: false}});
            console.log(response.data.data);
            if(response.data.error) {
                console.log(response.data.message);
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                }
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: UPDATE_QUEUE, payload: {queuedShows: response.data.data}});
            }
        });
    }
}

export const getRecent = (sessionId) => {
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/recently_watched.0.json?session_id=" + sessionId;
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "recent", status: true}});
        axios.get(builtUrl)
        .then(response => {
            console.log(response.data.data);
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "recent", status: false}});
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                }
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: UPDATE_RECENT, payload: {recentShows: response.data.data}});
            }
        });
    }
}

export const getBrowse = (sessionId, filter, limit, offset) => {
    return(dispatch) => {
        const limit2 = limit ? limit : "";
        const offset2 = offset ? offset : "";
        const builtUrl = "https://api.crunchyroll.com/list_series.0.json?session_id=" + sessionId + "&limit=" + limit2 + "&offset=" + offset2 + "&media_type=anime&filter=" + filter + "&locale=enUS";
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "browse", status: true}});
        axios.get(builtUrl)
        .then(response => {
            console.log(response.data.data);
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "browse", status: false}});
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                }
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: UPDATE_BROWSE, payload: {action: "add", showList: response.data.data}});
            }
        });
    }
}

export const clearBrowse = () => {
    return(dispatch) => {
        dispatch({type: UPDATE_BROWSE, payload: {action: "clear", showList: []}})
    }
}

export const loadSeries = (series, sessionId) => {
    console.log("loadSeries called");
    return(dispatch) => {
        dispatch({
            type: LOAD_SERIES,
            payload: series
        });
        dispatch({type: UPDATE_SELECTED_MODE, payload: "series"});
        dispatch(loadCollections(series, sessionId));
        //dispatch({type: UPDATE_NAVIGATION, payload: {}});
    }
}

export const loadCollections = (series, sessionId) => {
    console.log("LOAD COLLECTIONS");
    return(dispatch) => {
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: true}});
        const builtUrl = "https://api.crunchyroll.com/list_collections.0.json?session_id=" + sessionId + "&series_id=" + series.series_id + "&limit=9999"; //Add high limit to fetch all
        axios.get(builtUrl)
        .then(response => {
            console.log(response.data.data);
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});;
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                }
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: LOAD_COLLECTIONS_LIST, payload: response.data.data});
                var filteredCollectionList = [];
                response.data.data.map((collection) => {
                    //dispatch(loadCollectionMediaList(collection.collection_id, sessionId, collection.name, collection.season));
                    var name = collection.name;
                    var mediaLocale = "enUS";
                    //There does not appear to be a locale listing for regional trnslations in the api repsonse, so assume locale based on dub/sub info text
                    if(name.includes("Spanish")) {
                        //Spanish locale required to play media
                        mediaLocale = "esLA";
                    } else if(name.includes("German")) {
                        mediaLocale = "deDE";
                    } else if(name.includes("Russian")) {
                        mediaLocale = "ruRU";
                    } else if(name.includes("Portuguese")) {
                        mediaLocale = "ptPT";
                    } else if(name.includes("French")) {
                        mediaLocale = "frFR";
                    } else if(name.includes("Italian")) {
                        mediaLocale = "itIT";
                    }

                    if(mediaLocale === "enUS") {
                        filteredCollectionList.push(collection);
                    };

                    return 0;
                });

                dispatch(loadCollectionMediaList(filteredCollectionList, sessionId));
            }
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: false}});
        });
    }
}

export const loadCollectionMediaList = (collections, sessionId) => {
    console.log("LOAD COLLECTION MEDIA LIST");
    // var collectionsToFetch = [];
    return(dispatch) => {
    //     dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: true}});
    //     var mediaLocale = "enUS";
    //     //There does not appear to be a locale listing for regional trnslations in the api repsonse, so assume locale based on dub/sub info text
    //     if(name.includes("Spanish")) {
    //         //Spanish locale required to play media
    //         mediaLocale = "esLA";
    //     } else if(name.includes("German")) {
    //         mediaLocale = "deDE";
    //     } else if(name.includes("Russian")) {
    //         mediaLocale = "ruRU";
    //     } else if(name.includes("Portuguese")) {
    //         mediaLocale = "ptPT";
    //     } else if(name.includes("French")) {
    //         mediaLocale = "frFR";
    //     } else if(name.includes("Italian")) {
    //         mediaLocale = "itIT";
    //     }

    //     if( mediaLocale === "enUS")
    //     {
            var batchRequest = [];
            var collectionIdMap = [];
            for(var i = 0; i < collections.length; i++)
            {
                var request = {
                    "method_version":0,
                    "api_method":"list_media",
                    "params":{"collection_id":collections[i].collection_id, "limit":9999}
                };
                batchRequest.push(request);
                collectionIdMap.push({id: collections[i].collection_id, name: collections[i].name, season: collections[i].season});
            }
            const builtUrl = "https://api.crunchyroll.com/batch.0.json?session_id=" + sessionId + "&requests=" + JSON.stringify(batchRequest); //Add high limit to fetch all episodes
            axios.get(builtUrl)
            .then(response => {
                console.log(response.data.data);
                if(response.data.error) {
                    if (response.data.code === "bad_session") {
                        dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                        dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                    } else {
                        dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                    }
                }
                else {
                    dispatch({type: ERROR, payload: {isError: false, message: null}});
                    for(var i = 0; i < response.data.data.length; i++)
                    {
                        var collectionName = "";
                        var seasonNumber = 0;
                        for (var j = 0; j < collectionIdMap.length; j++)
                        {
                            if(collectionIdMap[j].id === response.data.data[i].body.data[0].collection_id)
                            {
                                collectionName = collectionIdMap[j].name;
                                seasonNumber = collectionIdMap[j].season;
                                break;
                            }
                        }
                        dispatch({type: LOAD_MEDIA_LIST, payload: {collectionName: collectionName, seasonNumber: seasonNumber, data:response.data.data[i].body.data}});
                        console.log("DISPATCH TO LOAD MEDIA RETURNED AT INDEX " + i);
                    }
                    dispatch({type: UPDATE_NAVIGATION, payload: {}});
                    console.log(response.data.data);
                }
                dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: false}});
            });
        //}
    }
}

export const clearMediaList = () => {
    return {type: CLEAR_MEDIA_LIST, payload: {}};
}

export const loadMediaList = (seriesId, sessionId) => {
    console.log("LOAD MEDIA LIST");
    return(dispatch) => {
        dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: true}});
        const builtUrl = "https://api.crunchyroll.com/list_media.0.json?session_id=" + sessionId + "&series_id=" + seriesId + "&limit=9999&locale=enUS"; //Add high limit to fetch all episodes
        axios.get(builtUrl)
        .then(response => {
            console.log(response.data.data);
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});;
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data.message}});
                }
            }
            else {
                dispatch({type: ERROR, payload: {isError: false, message: null}});
                dispatch({type: LOAD_MEDIA_LIST, payload: response.data.data});
            }
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "episodes", status: false}});
        });
    }
}

export const loadVideo = (sessionId, deviceId, deviceType, mediaId, locale) => {
    console.log("!!!loadVideo CALLED!!!");
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/info.0.json?session_id=" + sessionId + "&locale=" + locale + "&device_id=" + deviceId + "&device_type=" + deviceType + "&media_id=" + mediaId + "&fields=media.stream_data%2Cmedia.playhead";
        axios.get(builtUrl)
        .then(response => {
            console.log("STREAM REQUEST RESPONSE");
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                }
            }
            else {
                console.log(response.data);
                dispatch({type: LOAD_VIDEO, payload: {streamData: response.data, mediaId: mediaId}});
            }
        })
    }
}

export const unloadVideo = () => {
    console.log("!!! unloadVideo CALLED !!!");
    return ({type: UNLOAD_VIDEO, payload: {}});
}

export const addToHistory = (sessionId, mediaId) => {
    console.log("!!!addToHistory CALLED!!!");
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/log.0.json?session_id=" + sessionId + "&media_id=" + mediaId + "&event=playback_status";
        axios.get(builtUrl)
        .then(response => {
            console.log("addToHistory REQUEST RESPONSE");
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                }
            }
            else {
                console.log(response.data);
            }
        })
    }
}

export const addToQueue = (sessionId, seriesId) => {
    console.log("!!!addToQueue CALLED!!!");
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/add_to_queue.0.json?session_id=" + sessionId + "&series_id=" + seriesId;
        axios.post(builtUrl)
        .then(response => {
            console.log("addToQueue REQUEST RESPONSE");
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                }
            }
            else {
                console.log(response.data);
                dispatch(getQueue(sessionId));
            }
        })
    }
}

export const removeFromQueue = (sessionId, seriesId) => {
    console.log("!!!addToQueue CALLED!!!");
    return(dispatch) => {
        const builtUrl = "https://api.crunchyroll.com/remove_from_queue.0.json?session_id=" + sessionId + "&series_id=" + seriesId;
        axios.post(builtUrl)
        .then(response => {
            console.log("removeFromQueue REQUEST RESPONSE");
            if(response.data.error) {
                if (response.data.code === "bad_session") {
                    dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                    dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                } else {
                    dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                }
            }
            else {
                console.log(response.data);
                dispatch(getQueue(sessionId));
            }
        })
    }
}

export const search = (sessionId, searchText) => {
    console.log("!!!search CALLED!!!");
    return(dispatch) => {
        if(searchText !== "")
        {
            dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "search", status: true}});
            const builtUrl = "https://api.crunchyroll.com/autocomplete.0.json?session_id=" + sessionId + "&media_types=anime&q=" + searchText + "&limit=9999&locale=enUS";
            axios.post(builtUrl)
            .then(response => {
                console.log("search REQUEST RESPONSE");
                dispatch({type: UPDATE_LOADING_STATUS, payload: {name: "search", status: false}});
                if(response.data.error) {
                    if (response.data.code === "bad_session") {
                        dispatch({type: LOGOUT_USER, payload: {auth: null, account: null, password: null, isLoggedIn: false, sessionId: null}});
                        dispatch({type: ERROR, payload: {isError: true, message: "You have been logged out due to inactivity."}});
                    } else {
                        dispatch({type: ERROR, payload: {isError: true, message: response.data}});
                    }
                }
                else {
                    console.log(response.data);
                    dispatch({type: UPDATE_SEARCH_RESULTS, payload: response.data.data});
                }
            });
        } else {
            dispatch({type: UPDATE_SEARCH_RESULTS, payload: []});
        }
    }
}

export const setSearchbarFocus = (isFocused) => {
    return {
        type: SET_SEARCHBAR_FOCUS,
        payload: isFocused
    }
}

export const goBack = () => {
    return {
        type: GO_BACK,
        payload: {}
    }
}

export const setErrorState = (isError, message) => {
    console.log("!!!!SET ERROR STATE ACTION CALLED!!!!");
    return {
        type: ERROR,
        payload: {isError: isError, message: message}
    }
}
export const UPDATE_ACCOUNT = 'update_account';

export const UPDATE_LOGGED_IN = 'update_logged_in';

export const UPDATE_PASSWORD = 'update_password';

export const UPDATE_SESSION = "update_session";

export const LOGIN_USER = "login_user";

export const UPDATE_AUTH = "update_auth";

export const UPDATE_SELECTED_MODE = "update_selected_nodes";

export const TOGGLE_MENU = "toggle_menu";

export const LOGOUT_USER = "logout_user";

export const ERROR = "error";

export const UPDATE_QUEUE = "update_queue";

export const LOAD_SERIES = "load_series";

export const LOAD_MEDIA_LIST = "load_media_list";

export const LOAD_VIDEO = "load_video";

export const UNLOAD_VIDEO = "unload_video";

export const RESET_STATE = "reset_state";

export const UPDATE_NAVIGATION = "update_navigation";

export const GO_BACK = "go_back";

export const UPDATE_RECENT = "update_recent";

export const UPDATE_BROWSE = "update_browse";

export const UPDATE_LOADING_STATUS = "update_loading_status";

export const ADD_TO_HISTORY = "add_to_history";

export const ADD_TO_QUEUE = "add_to_queue";

export const REMOVE_FROM_QUEUE = "remove_from_queue";

export const UPDATE_SEARCH_RESULTS = "update_search_results";

export const SET_SEARCHBAR_FOCUS = "set_searchbar_focus";

export const LOAD_COLLECTIONS_LIST = "load_collections_list";

export const CLEAR_MEDIA_LIST = "clear_media_list";
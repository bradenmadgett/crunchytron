const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

function createWindow() {
  var screenElectron = electron.screen;
  var mainScreen = screenElectron.getPrimaryDisplay();
  var startWidth = mainScreen.size.width * .75;
  var startHeight = mainScreen.size.height * .8;
  mainWindow = new BrowserWindow({width: startWidth, height: startHeight, frame: false, webPreferences: {
    nodeIntegration: true
  }});
  mainWindow.loadURL(isDev ? 'http://localhost:3000' : path.resolve(__dirname, '../build/index.html'));
  console.log(path.resolve(__dirname, '../build/index.html'));
  mainWindow.setMenuBarVisibility(false);
  if (isDev) {
    // Open the DevTools.
    //BrowserWindow.addDevToolsExtension('<location to your react chrome extension>');
    mainWindow.webContents.openDevTools();
  }
  mainWindow.on('closed', () => mainWindow = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});